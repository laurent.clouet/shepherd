#!/usr/bin/env python3
#
# Copyright (C) 2015 Laurent CLOUET
# Author Laurent CLOUET <laurent.clouet@nopnop.net>
#
from OpenImageIO import ImageBuf, ImageBufAlgo
import sys
import re

def main(argv):
	if len(argv) != 3:
		raise SystemExit("Usage: %s src dest" % argv[0])

	def choose_channels(image, regex):
		indices = []
		for index, name in enumerate(image.spec().channelnames):
			match = re.fullmatch(regex, name)
			if match and match.group(1) in ("R", "G", "B", "A"):
				indices.append(index)
		return indices

	hdr = ImageBuf(argv[1])

	# Try to find channels named `Composite.Combined.`. Failing that, any `.Combined.` that starts with `View`. Failing THAT, any `.Combined.`
	channels = choose_channels(hdr, r"^Composite.Combined\.(.)$")
	if not channels:
		channels = choose_channels(hdr, r"^View.*\.Combined\.(.)$")
	if not channels:
		channels = choose_channels(hdr, r"^.*\.Combined\.(.)$")
	if not channels:
		raise Exception(f"No Channels with proper names found, available channels:\n{hdr.spec().channelnames}")

	# Remove any extraneous channels. OIIO automatically keeps them in RGBA order, so we just need to specify which ones we want
	stripped = ImageBufAlgo.channels(hdr, tuple(channels))

	# Color space conversion
	ldr = ImageBufAlgo.colorconvert(stripped, "linear", "sRGB")

	# Save
	ldr.write(argv[2])

if __name__ == "__main__":
	main(sys.argv)