<?php

namespace App\Command;

use App\Repository\TaskRepository;
use App\Service\MasterService;
use App\Service\TaskService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * A console command that resets all timed out tasks.
 *
 *     $ php bin/console shepherd:resettasks
 *
 */
class TaskResetScheduler extends Command {
    public static $defaultName = 'shepherd:resettasks';

    private EntityManagerInterface $entityManager;
    private TaskRepository $taskRepository;
    private TaskService $taskService;
    private MasterService $masterService;

    public function __construct(EntityManagerInterface $entityManager, MasterService $masterService, TaskRepository $taskRepository, TaskService $taskService) {
        parent::__construct();

        $this->entityManager = $entityManager;
        $this->masterService = $masterService;
        $this->taskRepository = $taskRepository;
        $this->taskService = $taskService;
    }

    protected function configure() : void {
        $this->setDescription('Resets all timed out tasks');
    }

    protected function execute(InputInterface $input, OutputInterface $output) : int {
        foreach ($this->taskRepository->findDeadTasks() as $task) {
            $this->taskService->reset($task);
            $this->masterService->notifyTaskReset($task);
        }
        $this->entityManager->flush();

        return Command::SUCCESS;
    }
}
