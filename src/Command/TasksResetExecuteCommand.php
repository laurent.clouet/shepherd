<?php

namespace App\Command;

use App\Entity\Task;
use App\Repository\TaskRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Reset all the rendering tasks
 *
 */
class TasksResetExecuteCommand extends Command {
    protected static $defaultName = 'shepherd:tasksreset';

    private EntityManagerInterface $entityManager;
    private TaskRepository $taskRepository;

    public function __construct(EntityManagerInterface $entityManager, TaskRepository $taskRepository) {
        parent::__construct();

        $this->entityManager = $entityManager;
        $this->taskRepository = $taskRepository;
}

    protected function configure() : void {
        $this->setDescription('Reset running tasks');
    }

    /**
     * This method is executed after initialize(). It usually contains the logic
     * to execute to complete this command task.
     */
    protected function execute(InputInterface $input, OutputInterface $output) : int {
       foreach ($this->taskRepository->findBy(['status' => Task::STATUS_RUNNING]) as $task) {
            $task->setStatus(Task::STATUS_WAITING);
        }
        $this->entityManager->flush();

        return Command::SUCCESS;
    }
}
