<?php

namespace App\Command;

use App\Entity\Task;
use App\Monitoring\MonitoringCpu;
use App\Repository\TaskRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\Lock\LockFactory;
use Symfony\Component\Lock\Store\FlockStore;

class DebugCommand extends Command {
    protected static $defaultName = 'shepherd:debug';

    private ContainerBagInterface $containerBag;
    private TaskRepository $taskRepository;

    protected function configure() : void {
        $this->setDescription('Show debug stats');
    }

    public function __construct(ContainerBagInterface $containerBag, TaskRepository $taskRepository) {
        parent::__construct();

        $this->containerBag = $containerBag;
        $this->taskRepository = $taskRepository;
    }

    /**
     * This method is executed after initialize(). It usually contains the logic
     * to execute to complete this command task.
     */
    protected function execute(InputInterface $input, OutputInterface $output) : int {
        echo "----------- CPU -----------\n";
        echo "load        ".(new MonitoringCpu())->getHumanValue()."\n";


        /**
         * @var FlockStore
         */
        $store = new FlockStore(sys_get_temp_dir());
        /**
         * @var LockFactory
         */
        $lockFactory = new LockFactory($store);

        $concurrent_tasks = (int)($this->containerBag->get('concurrent_tasks'));
        echo "------- Runners  ---------\n";
        for ($i = 1; $i <= $concurrent_tasks; $i++) {
            $lock = $lockFactory->createLock("shepherd.task.$i.pid");
            $lock->acquire(false);
            echo sprintf("%2d => %s", $i, $lock->isAcquired() ? 'Free' : 'Acquired');
            if ($lock->isAcquired()) {
                $lock->release();
            }
            echo "\n";
        }


        echo "----------- Tasks --------\n";
        echo "total: ".number_format($this->taskRepository->count([]))."\n";
        echo "distinct blends: ".number_format(count($this->taskRepository->getDistinctBlends()))."\n";
        echo "running blends: ".number_format(count($this->taskRepository->getDistinctRunningBlends()))."\n";
        echo "Running tasks:  ".number_format($this->taskRepository->count(['status' => Task::STATUS_RUNNING]))."\n";
        foreach ($this->taskRepository->findBy(['status' => Task::STATUS_RUNNING]) as $t) {
            echo "blend: ".$t->getBlend()->getId()." type: ".$t->getType()."\n";
        }
        echo "--------------------------\n";


        return Command::SUCCESS;
    }

}
