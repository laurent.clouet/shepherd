<?php


namespace App\Monitoring;


class MonitoringCpu extends MonitoringComponentAbstract {

    public function getType() : string {
        return 'cpu';
    }

    public function getValue() : float {
        $load = sys_getloadavg(); // last 1, 5 and 15 minutes.
        return $load[0];
    }
    
    public function isOverloaded() : bool {
        return $this->getValue() >= $this->getMax();
    }

    public function getHumanValue() : string {
        return $this->getValue().' / '.$this->getMax();
    }

    public function hasMax() : bool {
        return true;
    }

    public function getMax() : float {
        return (float)(rtrim(shell_exec('nproc')));
    }
}
