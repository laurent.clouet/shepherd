<?php


namespace App\Monitoring;


use App\Entity\Task;
use App\Tool\Size;
use Doctrine\ORM\EntityManagerInterface;

class MonitoringTask extends MonitoringComponentAbstract {
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager) {
        $this->entityManager = $entityManager;
    }


    public function getType() : string {
        return 'task';
    }

    public function getValue() : float {
        $taskEntity = $this->entityManager->getRepository(Task::class);
        return count($taskEntity->findAll());
    }

    public function getHumanValue() : string {
        return strval($this->getValue());
    }

}
