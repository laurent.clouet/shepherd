<?php


namespace App\Service;

use App\Entity\Frame;
use App\Entity\Task;
use App\Entity\Tile;
use App\Image;
use App\Repository\BlendRepository;
use App\Repository\FrameRepository;
use App\Repository\TileRepository;
use Doctrine\ORM\EntityManagerInterface;

class FrameService {
    private EntityManagerInterface $entityManager;
    private TileRepository $tileRepository;
    private BlendRepository $blendRepository;
    private BlendService $blendService;
    private FrameRepository $frameRepository;

    public function __construct(EntityManagerInterface $entityManager, BlendRepository $blendRepository, BlendService $blendService, TileRepository $tileRepository, FrameRepository $frameRepository) {
        $this->entityManager = $entityManager;
        $this->blendRepository = $blendRepository;
        $this->tileRepository = $tileRepository;
        $this->frameRepository = $frameRepository;
        $this->blendService = $blendService;
    }

    public function onFinish(Frame $frame) {
        $task = new Task();
        $task->setType(Task::TYPE_GENERATE_FRAME);
        $task->setFrame($frame);
        $task->setBlend($frame->getBlend());
        $this->entityManager->persist($task);

        $this->entityManager->flush();

        if ($this->blendRepository->isFinished($frame->getBlend())) {
            $this->blendService->onFinish($frame->getBlend());
        }
    }

    public function generateImageThumbnail(Frame $frame) : bool {
        $thumbnailPath = $this->frameRepository->getThumbnailPath($frame);


        $tempPath = sys_get_temp_dir().'/'.uniqid().'.png';

        // TODO, not really optimized
        if ($frame->getType() == 'full') {
            // let's check if the actual thumbnail of the "100% tile" exists, if so no need for a transform
            if ($frame->getTiles()->isEmpty() == false) {
                $tile = $frame->getTiles()->first();
                $tile_path = $this->tileRepository->getThumbnailPath($tile);
                if (file_exists($tile_path)) {
                    // do nothing :)
                    return true;
                }
            }
            if ($this->generateImageFull($frame, $tempPath) == false) {
                return false;
            }
        }
        elseif ($frame->getType() == 'layer') {
            if ($this->generateImageLayer($frame, $tempPath) == false) {
                return false;
            }
        }
        elseif ($frame->getType() == 'region') {
            if ($this->generateImageRegion($frame, $tempPath) == false) {
                return false;
            }
        }
        else {
            return false;
        }

        $img = new Image($tempPath);
        $ret = $img->generateThumbnail(200, 0, $thumbnailPath);
        @unlink($tempPath);

        $this->blendService->updateSize($frame->getBlend(), filesize($thumbnailPath));
        return $ret;
    }

    public function generateImage(Frame $frame) : bool {
        $ret = false;
        if ($frame->getType() == 'full' || $frame->getTiles()->count() == 1) {
            $ret =  $this->generateImageFull($frame);
        }
        elseif ($frame->getType() == 'layer') {
            $ret =  $this->generateImageLayer($frame);
        }
        elseif ($frame->getType() == 'region') {
            $ret = $this->generateImageRegion($frame);
        }
        else {
            return false;
        }

        $this->blendService->updateSize($frame->getBlend(), filesize($this->frameRepository->getFullPath($frame)));
        return $ret;
    }

    private function generateImageFull(Frame $frame, string $targetPath = null) : bool {
        // frame have 1 tile which is 100% of the output
        if ($frame->getTiles()->isEmpty()) {
            return false;
        }
        $tile = $frame->getTiles()->first();
        $tilePath = $this->tileRepository->getPath($tile);
        if (is_null($targetPath)) {
            $framePath = $this->frameRepository->getFullPath($frame);
        }
        else {
            $framePath = $targetPath;
        }

        return copy($tilePath, $framePath);
    }

    private function generateImageRegion(Frame $frame, string $targetPath = null) : bool {
        if ($frame->getTiles()->isEmpty()) {
            return false;
        }

        $sqrt = (int)sqrt($frame->getTiles()->count());
        if ($frame->getTiles()->count() != ($sqrt * $sqrt)) {
            // only support square count (1,4,9,25,36,...
            return false;
        }

        if (is_null($targetPath)) {
            $framePath = $this->frameRepository->getFullPath($frame);
        }
        else {
            $framePath = $targetPath;
        }

        $files = array();
        foreach ($frame->getTiles() as $tile) {
            $tilePath = $this->tileRepository->getPath($tile);

            $x = ((int)($tile->getNumber() / $sqrt));
            $y = ((int)($tile->getNumber() % $sqrt));

            if (array_key_exists($y, $files) == false) {
                $files[$y] = array();
            }
            $files[$y][$x] = $tilePath;
        }

        $files = array_reverse($files);

        $img = new Image($framePath);
        return $img->assemble($files);
    }

    /**
     * The first tile is the background
     * The second frame is mixed with 50% opacity
     * The third frame is mixed with 33% opacity
     * And so on, each Nth frame is mixed with 100/n % opacity
     */
    private function generateImageLayer(Frame $frame, string $targetPath = null) : bool {
        if ($frame->getTiles()->isEmpty()) {
            return false;
        }
        if (is_null($targetPath)) {
            $framePath = $this->frameRepository->getStorageDirectory($frame).DIRECTORY_SEPARATOR.sprintf("%04d", $frame->getNumber()).'.'.$frame->getImageExtension();
        }
        else {
            $framePath = $targetPath;
        }

        $files = array();
        foreach ($frame->getTiles() as $tile) {
            if ($tile->getStatus() == Tile::STATUS_FINISHED) {
                $tilePath = $this->tileRepository->getPath($tile);
                if (file_exists($tilePath)) {
                    $files [] = $tilePath;
                }
            }
        }


        $img = new Image($framePath);
        return $img->assembleFromSplit($files);
    }

    /**
     * @return int
     */
    public function getWidthPerTile(?Frame $frame) {
        if (is_null($frame)) {
            return -1;
        }
        $blend = $frame->getBlend();
        if (is_null($blend)) {
            return -2;
        }
        if ($blend->getHeight() <= 0 || $blend->getWidth() <= 0) {
            return -3;
        }

        if ($frame->getType() == 'full') {
            return $blend->getWidth();
        }
        elseif ($frame->getType() == 'layer') {
            return $blend->getWidth();
        }
        elseif ($frame->getType() == 'region') {
            if ($frame->getTiles()->isEmpty()) {
                return -3;
            }

            $sqrt = (int)sqrt($frame->getTiles()->count());
            if ($frame->getTiles()->count() != ($sqrt * $sqrt)) {
                // only support square count (1,4,9,25,36,...
                return -4;
            }

            return $blend->getWidth() / $sqrt;
        }

        return -5;
    }

    public function getHeightPerTile(?Frame $frame) {
        if (is_null($frame)) {
            return -1;
        }
        $blend = $frame->getBlend();
        if (is_null($blend)) {
            return -2;
        }
        if ($frame->getType() == 'full') {
            return $blend->getHeight();
        }
        elseif ($frame->getType() == 'layer') {
            return $blend->getHeight();
        }
        elseif ($frame->getType() == 'region') {
            if ($frame->getTiles()->isEmpty()) {
                return -3;
            }

            $sqrt = (int)sqrt($frame->getTiles()->count());
            if ($frame->getTiles()->count() != ($sqrt * $sqrt)) {
                // only support square count (1,4,9,25,36,...
                return -4;
            }

            return round($blend->getHeight() / $sqrt);
        }

        return -5;
    }
}