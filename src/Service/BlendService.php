<?php


namespace App\Service;


use App\Entity\Blend;
use App\Entity\Frame;
use App\Entity\Task;
use App\Entity\Tile;
use App\Image;
use App\Repository\BlendRepository;
use App\Repository\FrameRepository;
use App\Repository\TaskRepository;
use App\Tool\Size;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use PhpZip\Exception\ZipException;
use PhpZip\Exception\InvalidArgumentException;
use PhpZip\ZipFile;

class BlendService {
    private EntityManagerInterface $entityManager;
    private FrameRepository $frameRepository;
    private BlendRepository $blendRepository;
    private TaskRepository $taskRepository;

    public function __construct(EntityManagerInterface $entityManager, BlendRepository $blendRepository, FrameRepository $frameRepository, TaskRepository $taskRepository) {
        $this->entityManager = $entityManager;
        $this->frameRepository = $frameRepository;
        $this->blendRepository = $blendRepository;
        $this->taskRepository = $taskRepository;
    }

    public function onFinish(Blend $blend) {
        if ($blend->getGenerateMp4()) {
            $taskMP4Final = new Task();
            $taskMP4Final->setType(Task::TYPE_GENERATE_MP4_FINAL);
            $taskMP4Final->setBlend($blend);

            $taskMP4Preview = new Task();
            $taskMP4Preview->setType(Task::TYPE_GENERATE_MP4_PREVIEW);
            $taskMP4Preview->setBlend($blend);

            $this->entityManager->persist($taskMP4Final);
            $this->entityManager->persist($taskMP4Preview);
        }

        $taskZip = new Task();
        $taskZip->setType(Task::TYPE_GENERATE_ZIP);
        $taskZip->setBlend($blend);

        $this->entityManager->persist($taskZip);
        $this->entityManager->flush();

    }

    public function generateMP4Preview(Blend $blend) : bool {
        $root = $this->blendRepository->getStorageDirectory($blend);
        $mp4Path = $root.DIRECTORY_SEPARATOR.'output'.DIRECTORY_SEPARATOR.$blend->getId().'_preview.mp4'; // use a better name 444.zip is not humanfriendly

        $mp4TempPath = $mp4Path.'_partial.mp4';
        @unlink($mp4Path);
        @unlink($mp4TempPath);

        $fps = $blend->getFramerate();

        $framesDirectory = $this->blendRepository->getStorageDirectory($blend).DIRECTORY_SEPARATOR.'frames';
        $firstFrame = $blend->getFrames()->first();
        $picture_extension = $firstFrame->getImageExtension();

        // get geometry
        $firstFramePath = $this->frameRepository->getStorageDirectory($firstFrame).DIRECTORY_SEPARATOR.sprintf("%04d", $firstFrame->getNumber()).'.'.$firstFrame->getImageExtension();
        $anImage = new Image($firstFramePath);
        $geometry = $anImage->getGeometry();

        $maxWidth = 600;
        if ($geometry['width'] > $maxWidth) {
            $height = 4 * (int)(($maxWidth * $geometry['height'] / $geometry['width']) / 4);
            $width = $maxWidth;
            $geometry = array('width' => $width, 'height' => $height);
        }

        $min = PHP_INT_MAX;
        foreach ($blend->getFrames() as $f) {
            $min = min($min, $f->getNumber());
        }

        // don't user 0000 as first image because ffmpeg doesn't like it
        // ffmpeg version 11.7-6:11.7-1~deb8u1, Copyright (c) 2000-2016 the Libav developers
        // built on Jun 12 2016 21:51:35 with gcc 4.9.2 (Debian 4.9.2-10)
        // [image2 demuxer @ 0xc13760] Value 0.000000 for parameter 'start_number' out of range
        // [image2 demuxer @ 0xc13760] Error setting option start_number to value 0000.
        // %4d.png: Numerical result out of range
        if ($min == '0000') {
            $min = '0001';
        }

        // -c:v libx264 — the video codec is libx264 (H.264).
        // -profile:v high — use H.264 High Profile (advanced features, better quality).
        // -crf 20 — constant quality mode, very high quality (lower numbers are higher quality, 18 is the smallest you would want to use).
        // -pix_fmt yuv420p — use YUV pixel format and 4:2:0 Chroma subsampling

        chdir($framesDirectory);

        $cmd = "ffmpeg -threads 2 -r $fps -start_number $min -i \"%4d.$picture_extension\" -s ".$geometry['width']."x".$geometry['height']." -vf \"drawtext=fontfile=Arial.ttf: text='%{frame_num}': start_number=".$min.": x=(w-tw)/2: y=h-(2*lh): fontcolor=black: fontsize=100: box=1: boxcolor=white: boxborderw=10\" -c:v libx264 -crf 18 -pix_fmt yuv420p -color_primaries bt709 -color_trc bt709 -colorspace bt709 -x264-params sliced-threads=1 $mp4TempPath";
        exec($cmd.' 2>&1', $output);

        $this->reconnectEntityManager();

        $this->updateSize($blend, filesize($mp4TempPath));
        return @rename($mp4TempPath, $mp4Path);
    }

    public function generateMP4Final(Blend $blend) : bool {
        $root = $this->blendRepository->getStorageDirectory($blend);
        $mp4Path = $root.DIRECTORY_SEPARATOR.'output'.DIRECTORY_SEPARATOR.$blend->getId().'_final.mp4'; // use a better name 444.zip is not humanfriendly

        $mp4TempPath = $mp4Path.'_partial.mp4';
        if (file_exists($mp4Path)) {
            @unlink($mp4Path);
        }
        if (file_exists($mp4TempPath)) {
            @unlink($mp4TempPath);
        }

        $fps = $blend->getFramerate();

        $framesDirectory = $this->blendRepository->getStorageDirectory($blend).DIRECTORY_SEPARATOR.'frames';
        $firstFrame = $blend->getFrames()->first();
        $picture_extension = $firstFrame->getImageExtension();

        chdir($framesDirectory);
        $cmd = "ffmpeg -threads 2 -r $fps -pattern_type glob -i \"*.$picture_extension\" -c:v libx264 -profile:v high -crf 20 -preset slower -maxrate 10M -bufsize 20M -pix_fmt yuv420p -color_primaries bt709 -color_trc bt709 -colorspace bt709 -x264-params sliced-threads=1 $mp4TempPath";

        exec($cmd.' 2>&1', $output);

        $this->reconnectEntityManager();

        $this->updateSize($blend, filesize($mp4TempPath));
        @rename($mp4TempPath, $mp4Path);
        return file_exists($mp4Path);
    }

    public function generateZIP(Blend $blend) : bool {
        $root = $this->blendRepository->getStorageDirectory($blend);
        $framesDirectory = $root.DIRECTORY_SEPARATOR.'frames';
        $zipPath = $this->blendRepository->getZip($blend); // use a better name 444.zip is not humanfriendly

        $zipFile = new ZipFile();
        try {
            foreach (glob($framesDirectory.DIRECTORY_SEPARATOR.'*') as $path) {
                $zipFile->addFile($path); // add an entry from the file
            }
            $zipFile->saveAsFile($zipPath);
            $zipFile->close(); // close archive

        } catch (ZipException $e) {
            return false;
        } catch (InvalidArgumentException $e) { // if the target zipPath parent directory doesn't exist.
        } finally {
            $zipFile->close();
        }

        $this->reconnectEntityManager();

        $this->updateSize($blend, filesize($zipPath));
        return file_exists($zipPath);
    }

    /**
     * @return Tile[]
     */
    public function addBlend(int $id, string $image_extension, float $framerate, int $width, int $height, bool $mp4, array $frames, ?string $tokenOwner, ?DateTime $tokenOwnerValidity, ?string $tokenThumbnail, ?DateTime $tokenThumbnailValidity) {
        $tiles = array();

        $blend = new Blend();
        $blend->setId($id);
        $blend->setFramerate($framerate);
        $blend->setWidth($width);
        $blend->setHeight($height);
        $blend->setGenerateMp4($mp4);
        $blend->setSize(0);

        if ($tokenOwner != null && $tokenOwnerValidity != null) {
            $blend->setOwnerToken($tokenOwner);
            $blend->setOwnerTokenValidity($tokenOwnerValidity);
        }

        if ($tokenThumbnail != null && $tokenThumbnailValidity != null) {
            $blend->setThumbnailToken($tokenThumbnail);
            $blend->setThumbnailTokenValidity($tokenThumbnailValidity);
        }

        $this->entityManager->persist($blend);

        foreach ($frames as $frameJson) {
            $frame = new Frame();
            $frame->setType($frameJson['type']);
            $frame->setNumber((int)$frameJson['number']);
            $frame->setImageExtension($frameJson['image_extension']);
            $blend->addFrame($frame);
            $this->entityManager->persist($frame);
            if (array_key_exists('tiles', $frameJson) == false) {
                if ($frameJson['type'] == 'full') {
                    $tile = new Tile();
                    $tile->setNumber(0);
                    $tile->setId($frameJson['uid']);
                    $tile->setStatus(Tile::STATUS_PROCESSING);
                    $tile->setToken($frameJson['token']);
                    $frame->addTile($tile);

                    $this->entityManager->persist($tile);
                    $tiles [] = $tile;
                }
            }
            else {
                foreach ($frameJson['tiles'] as $tileJson) {
                    $tile = new Tile();
                    $tile->setNumber((int)$tileJson['number']);
                    $tile->setId($tileJson['uid']);
                    $tile->setStatus(Tile::STATUS_PROCESSING);
                    $tile->setToken($tileJson['token']);
                    $frame->addTile($tile);

                    $this->entityManager->persist($tile);
                    $tiles [] = $tile;
                }
            }
        }
        $this->entityManager->flush();

        $root = $this->blendRepository->getStorageDirectory($blend);
        @mkdir($root);
        @mkdir($root.DIRECTORY_SEPARATOR.'output');
        @mkdir($root.DIRECTORY_SEPARATOR.'frames');
        @mkdir($root.DIRECTORY_SEPARATOR.'thumbnails');
        @mkdir($root.DIRECTORY_SEPARATOR.'thumbnails'.DIRECTORY_SEPARATOR.'frames');
        @mkdir($root.DIRECTORY_SEPARATOR.'thumbnails'.DIRECTORY_SEPARATOR.'tiles');
        @mkdir($root.DIRECTORY_SEPARATOR.'tiles');
        @mkdir($root.DIRECTORY_SEPARATOR.'tiles'.DIRECTORY_SEPARATOR.'frames');

        return $tiles;
    }

    public function delBlend(Blend $blend) : bool {
        $root = $this->blendRepository->getStorageDirectory($blend);
        $this->rrmdir($root);

        foreach ($blend->getFrames() as $frame) {
            foreach ($frame->getTiles() as $tile) {
                $this->entityManager->remove($tile);
            }

            $this->entityManager->remove($frame);
        }

        foreach ($this->taskRepository->findBy(['blend' => $blend->getId()]) as $task) {
            $this->entityManager->remove($task);
        }

        $this->entityManager->remove($blend);
        $this->entityManager->flush();

        return true;
    }

    /**
     * Calculate storage size used by a blend
     */
    public function setSize(Blend $blend){
        $path = $this->blendRepository->getStorageDirectory($blend);
        $blend->setSize($size = Size::dirSize($path));
        $this->entityManager->flush();
    }

    /**
     * Update storage used by a blend
     * @param float $update add/remove to the current storage usage
     */
    public function updateSize(Blend $blend, float $update){
        $blend->setSize($blend->getSize() + $update);
        $this->entityManager->flush();
    }

    private function rrmdir($src) {
        if (file_exists($src)) {
            $dir = opendir($src);
            while (false !== ($file = readdir($dir))) {
                if (($file != '.') && ($file != '..')) {
                    $full = $src.'/'.$file;
                    if (is_dir($full)) {
                        $this->rrmdir($full);
                    }
                    else {
                        unlink($full);
                    }
                }
            }
            closedir($dir);
            rmdir($src);
        }
    }

    public function isOwnerTokenValid(Blend $blend, string $token) : bool {
        return $blend->getOwnerToken() == $token && new \DateTime() < $blend->getOwnerTokenValidity();
    }

    public function isThumbnailTokenValid(Blend $blend, string $token) : bool {
        return $blend->getThumbnailToken() == $token && new \DateTime() < $blend->getThumbnailTokenValidity();
    }

    /**
     * If a task takes a lot of time, it might be over the sql connection timeout
     */
    private function reconnectEntityManager() {
        if ($this->entityManager->getConnection()->ping() == false) {
            $this->entityManager->getConnection()->close();
            $this->entityManager->getConnection()->connect();
        }
    }
}
