<?php

namespace App\Controller;

use App\Entity\Blend;
use App\Entity\Tile;
use App\Repository\FrameRepository;
use App\Repository\TileRepository;
use App\Service\BlendService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller to handle request for thumbnail
 * @Route("/thumb")
 */
class ThumbnailController extends AbstractController {

    private BlendService $blendService;
    private FrameRepository $frameRepository;
    private TileRepository $tileRepository;

    public function __construct(
        BlendService $blendService,
        FrameRepository $frameRepository,
        TileRepository $tileRepository) {

        $this->blendService = $blendService;
        $this->frameRepository = $frameRepository;
        $this->tileRepository = $tileRepository;
    }

    /**
     * @Route("/{token}/{blend}/frame/{frameNumber}/thumbnail", methods="GET")
     */
    public function getFrameThumbnail(string $token, Blend $blend, int $frameNumber) : BinaryFileResponse {
        if ($this->blendService->isThumbnailTokenValid($blend, $token) == false) {
            throw $this->createNotFoundException('token not valid');
        }
        $frame = $this->frameRepository->findOneBy(array('blend' => $blend->getId(), 'number' => $frameNumber));
        if (is_object($frame)) {
            $path = $this->frameRepository->getThumbnailPath($frame);

            if (file_exists($path) == false) {
                throw $this->createNotFoundException('file not found');
            }

            return $this->giveFile($path);
        }
        else {
            throw $this->createNotFoundException('file not found');
        }
    }

    /**
     * @Route("/{token}/{blend}/tile/{tile}/thumbnail", methods="GET")
     */
    public function getTileThumbnail(string $token, Blend $blend, Tile $tile) : BinaryFileResponse {
        if ($this->blendService->isThumbnailTokenValid($blend, $token) == false) {
            throw $this->createNotFoundException('token not valid');
        }
        $path = $this->tileRepository->getThumbnailPath($tile);

        if (file_exists($path) == false) {
            throw $this->createNotFoundException('file not found');
        }

        return $this->giveFile($path);
    }

    private function giveFile(string $path) {
        try {
            $file = new File($path, true);
        } catch (FileNotFoundException $e) {
            throw $this->createNotFoundException('file not found');
        }

        header('Content-Type: '.$file->getMimeType());
        header('Content-Length: '.filesize($file));

        return $this->file($file, $file->getFilename(), ResponseHeaderBag::DISPOSITION_INLINE);
    }
}
