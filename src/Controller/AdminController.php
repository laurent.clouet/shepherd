<?php


namespace App\Controller;

use App\Monitoring\MonitoringCpu;
use App\Monitoring\MonitoringDisk;
use App\Monitoring\MonitoringHttpd;
use App\Monitoring\MonitoringRam;
use App\Repository\BlendRepository;
use App\Repository\TaskRepository;
use App\Tool\Size;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller to handle routes for admins
 *
 * @Route("/admin")
 */
class AdminController extends AbstractController {

    private ContainerBagInterface $containerBag;
    private BlendRepository $blendRepository;
    private TaskRepository $taskRepository;

    public function __construct(
        ContainerBagInterface $containerBag,
        BlendRepository $blendRepository,
        TaskRepository $taskRepository) {

        $this->containerBag = $containerBag;
        $this->blendRepository = $blendRepository;
        $this->taskRepository = $taskRepository;
    }

    /**
     * @Route("/status")
     */
    public function status() : Response {
        $blends = $this->blendRepository->findAll();
        foreach ($blends as $k => $blend) {
            $blend->disk_usage = Size::humanSize($blend->getSize());
        }

        $monitoring = array();
        foreach (array(new MonitoringHttpd(), new MonitoringCpu(), new MonitoringRam(), new MonitoringDisk($this->containerBag, $this->blendRepository)) as $monitor) {
            $data = array('type' => $monitor->getType(), 'value' => $monitor->getHumanValue());

            $monitoring [] = $data;
        }

        return $this->render('status.html.twig', [
            'version' => getenv('VERSION'),
            'tasks' => $this->taskRepository->findAll(),
            'monitoring' => $monitoring,
            'blends' => $blends
        ]);
    }
}