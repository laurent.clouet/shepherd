<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TileRepository")
 * @ORM\Table(name="tile")
 *
 */
class Tile {
    public const STATUS_WAITING = 'waiting';
    public const STATUS_PROCESSING = 'rendering';
    public const STATUS_FINISHED = 'rendered';
    public const STATUS_PAUSED = 'paused';


    /**
     * @ORM\Id()
     * @ORM\Column(type="string", length=40)
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @Assert\NotBlank
     */
    private $number;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $token;

    /**
     * @ORM\ManyToOne(targetEntity="Frame", inversedBy="tiles")
     * @ORM\JoinColumn(nullable=false)
     */
    private $frame;

    public function getId() : ?string {
        return $this->id;
    }

    public function setId(string $id) : void {
        $this->id = $id;
    }

    public function getNumber() : ?int {
        return $this->number;
    }

    public function setNumber(int $number) : self {
        $this->number = $number;

        return $this;
    }

    public function getStatus() : ?string {
        return $this->status;
    }

    public function setStatus(string $status) : self {
        $this->status = $status;

        return $this;
    }

    public function getToken() : ?string {
        return $this->token;
    }

    public function setToken(?string $token) : self {
        $this->token = $token;

        return $this;
    }

    public function getFrame() : ?Frame {
        return $this->frame;
    }

    public function setFrame(?Frame $frame) : self {
        $this->frame = $frame;

        return $this;
    }

    public function getImageExtension() : string {
        if ($this->frame->getType() == "full") {
            return $this->frame->getImageExtension();
        }
        else {
            return 'png';
        }
    }

    public function __toString() {
        return 'Tile(id: '.$this->id.' number: '.$this->number.' status: '.$this->status.' token: '.$this->token /* . ' startTime: ' . $this->startTime . ' validationTime: ' . $this->validationTime*/.')';
    }
}