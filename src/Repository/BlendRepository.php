<?php


namespace App\Repository;

use App\Entity\Blend;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;

class BlendRepository extends ServiceEntityRepository {
    private ContainerBagInterface $containerBag;
    private FrameRepository $frameRepository;

    public function __construct(ManagerRegistry $registry, ContainerBagInterface $containerBag, FrameRepository $frameRepository) {
        parent::__construct($registry, Blend::class);
        $this->containerBag = $containerBag;
        $this->frameRepository = $frameRepository;
    }

    public function isFinished(Blend $blend) : bool {
        foreach ($blend->getFrames() as $frame) {
            if ($this->frameRepository->isFinished($frame) == false) {
                return false;
            }
        }
        return true;
    }

    public function getStorageDirectory(Blend $blend) : string {
        return $this->containerBag->get('storage_dir') /*. DIRECTORY_SEPARATOR*/.$blend->getId();
    }

    public function getMP4FinalPath(Blend $blend) : string {
        return $this->getStorageDirectory($blend).DIRECTORY_SEPARATOR.'output'.DIRECTORY_SEPARATOR.$blend->getId().'_final.mp4';
    }

    public function getMP4PreviewPath(Blend $blend) : string {
        return $this->getStorageDirectory($blend).DIRECTORY_SEPARATOR.'output'.DIRECTORY_SEPARATOR.$blend->getId().'_preview.mp4';
    }

    public function getZip(Blend $blend) {
        return $this->getStorageDirectory($blend).DIRECTORY_SEPARATOR.'output'.DIRECTORY_SEPARATOR.$blend->getId().'.zip';
    }

}