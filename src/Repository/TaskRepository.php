<?php

namespace App\Repository;

use App\Entity\Task;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class TaskRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Task::class);
    }

    public function findOneAvailable() : ?Task {
        $runningBlends = [];
        foreach ($this->findBy(['status' => Task::STATUS_RUNNING]) as $t) {
            if ($t->getType() != Task::TYPE_GENERATE_TILE_THUMBNAIL && $t->getType() != Task::TYPE_GENERATE_FRAME_THUMBNAIL) { // thumb task can be work by multiple worker
                $runningBlends[$t->getBlend()->getId()] = $t->getBlend()->getId();
            }
        }

        // first, find high priority task
        $qb = $this->_em->createQueryBuilder()
            ->select('t')
            ->from($this->_entityName, 't')
            ->andWhere('t.status = :waiting')
            ->andWhere('t.type != :thumb_tile')
            ->andWhere('t.type != :thumb_frame')
            ->andWhere('t.type != :delete_blend')
            ->setParameter('thumb_tile', Task::TYPE_GENERATE_TILE_THUMBNAIL)
            ->setParameter('thumb_frame', Task::TYPE_GENERATE_FRAME_THUMBNAIL)
            ->setParameter('delete_blend', Task::TYPE_DELETE_BLEND)
            ->setParameter('waiting', Task::STATUS_WAITING);

        if (count($runningBlends) > 0) {
            $qb->andWhere($qb->expr()->notIn('t.blend', ':runningBlends'))
                ->setParameter('runningBlends', $runningBlends);
        }

        $qb->addOrderBy('t.id', 'ASC')
            ->setMaxResults(1);
        $result = $qb->getQuery()->getOneOrNullResult();


        if ($result != null) {
            return $result;
        }
        else {
            // looking for low priority tasks (i.e. thumbnails)
            $qb = $this->_em->createQueryBuilder()
                ->select('t')
                ->from($this->_entityName, 't')
                ->andWhere('t.status = :waiting')
                ->andWhere('t.type = :thumb_frame OR t.type = :thumb_tile OR t.type = :delete_blend')
                ->setParameter('thumb_tile', Task::TYPE_GENERATE_TILE_THUMBNAIL)
                ->setParameter('thumb_frame', Task::TYPE_GENERATE_FRAME_THUMBNAIL)
                ->setParameter('delete_blend', Task::TYPE_DELETE_BLEND)
                ->setParameter('waiting', Task::STATUS_WAITING)
                ->setMaxResults(1);

            return $qb->getQuery()->getOneOrNullResult();
        }
    }

    public function getDistinctRunningBlends() : array {
        $runningBlends = [];
        foreach ($this->findBy(['status' => Task::STATUS_RUNNING]) as $b) {
            $runningBlends[$b->getBlend()->getId()] = $b->getBlend()->getId();
        }
        return $runningBlends;
    }

    public function getDistinctBlends() : array {
        $blends = [];
        foreach ($this->findAll() as $b) {
            $blends[$b->getBlend()->getId()] = $b->getBlend()->getId();
        }
        return $blends;
    }

    /**
     * @return Task[]
     */
    public function findDeadTasks(): array {
        // TODO: do it in QDL instead of PHP, it will be faster :)
        $min =  (new \DateTime())->setTimestamp(time() - Task::MAX_RUNNING_TIME);
        $ret = [];
        foreach ($this->findBy(['status' => Task::STATUS_RUNNING]) as $task) {
            /** @var Task $task */
            if ($task->getStartTime() != null && $task->getStartTime() < $min) {
                $ret []= $task;
            }
        }
        return $ret;
    }
}
