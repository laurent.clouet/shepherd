<?php

namespace App\Tests;

use App\Image;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ImageTest extends KernelTestCase  {
    protected function setUp() : void {
        $kernel = self::bootKernel();
    }

    public function testExrIsImage() {
        $sourceEXR = __DIR__.'/data/render.exr';

        $image = new Image($sourceEXR);

        $this->assertTrue($image->isExr());
        $this->assertTrue($image->isImage());
    }

    public function testExrThumbnail() {
        $sourceEXR = __DIR__.'/data/render.exr';

        $image = new Image($sourceEXR);

        $target = sys_get_temp_dir().'/'.uniqid().'.png';

        $this->assertTrue($image->generateThumbnail(500, 250, $target));

        $this->assertFileExists($target);
        $thumb = new Image($target);
        $this->assertTrue($thumb->isImage());

        @unlink($target); // cleanup after the test
    }

    public function testTGAWithExtension() {
        $sourceTGA = __DIR__.'/data/render.tga';

        $image = new Image($sourceTGA);

        $target = sys_get_temp_dir().'/'.uniqid().'.png';

        $this->assertTrue($image->generateThumbnail(500, 250, $target));

        $this->assertFileExists($target);
        $thumb = new Image($target);
        $this->assertTrue($thumb->isImage());

        @unlink($target); // cleanup after the test
    }

    public function testTGAWithoutExtension() {
        $sourceTGA = __DIR__.'/data/render_tga';

        $image = new Image($sourceTGA);

        $target = sys_get_temp_dir().'/'.uniqid().'.png';

        $this->assertTrue($image->generateThumbnail(500, 250, $target));

        $this->assertFileExists($target);
        $thumb = new Image($target);
        $this->assertTrue($thumb->isImage());

        @unlink($target); // cleanup after the test
    }
}