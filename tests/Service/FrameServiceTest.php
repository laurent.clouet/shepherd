<?php


namespace App\Tests\Service;

use App\Entity\Blend;
use App\Repository\BlendRepository;
use App\Repository\FrameRepository;
use App\Repository\TileRepository;
use App\Service\BlendService;
use App\Service\FrameService;
use App\Service\TileService;
use App\Tests\Tools;
use App\Tests\ToolsService;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FrameServiceTest extends KernelTestCase {
    private EntityManager $entityManager;
    private TileService $tileService;
    private FrameService $frameService;
    private BlendService $blendService;
    private BlendRepository $blendRepository;
    private FrameRepository $frameRepository;
    private TileRepository $tileRepository;
    private ToolsService $toolsService;
    private Tools $tools;
    private Blend $blend;

    protected function setUp() : void {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()->get('doctrine')->getManager(); // self::$container
        $this->blendRepository = self::$container->get('App\Repository\BlendRepository');
        $this->frameRepository = self::$container->get('App\Repository\FrameRepository');
        $this->tileRepository = self::$container->get('App\Repository\TileRepository');
        $this->blendService = self::$container->get('App\Service\BlendService');
        $this->frameService = self::$container->get('App\Service\FrameService');
        $this->tileService = self::$container->get('App\Service\TileService');
        $this->toolsService = new ToolsService($this->blendService);
        $this->tools = new Tools($this->entityManager, $this->tileRepository, $this->frameRepository, $this->frameService);
    }

    public function testOnFinishTileForFrameFull() {
        $blendId = $this->toolsService->addBlend("full", 10, 1, 1920, 1080);
        $this->assertTrue(is_int($blendId));

        $this->blend = $this->blendRepository->find($blendId);
        $this->assertNotNull($this->blend);

        $frame = $this->blend->getFrames()->get(0);

        $this->tools->validateTile($frame->getTiles()->get(0), new UploadedFile($this->tools->generateFakeImage(256, 256), '0.png', 'image/png', null, true));

        $this->assertTrue($this->frameService->generateImage($frame));
        $this->assertFileExists($this->frameRepository->getStorageDirectory($frame).DIRECTORY_SEPARATOR.sprintf("%04d", $frame->getNumber()).'.'.$frame->getImageExtension());
    }

    public function testOnFinishTileForFrameRegion() {

        $blendId = $this->toolsService->addBlend("region", 1, 4, 1920, 1080);
        $this->assertTrue(is_int($blendId));

        $this->blend = $this->blendRepository->find($blendId);
        $this->assertNotNull($this->blend);

        $frame = $this->blend->getFrames()->get(0);
        $this->tools->validateTile($frame->getTiles()->get(0), new UploadedFile($this->tools->generateFakeImage(256, 256), '0.png', 'image/png', null, true));
        $this->tools->validateTile($frame->getTiles()->get(1), new UploadedFile($this->tools->generateFakeImage(256, 256), '0.png', 'image/png', null, true));
        $this->tools->validateTile($frame->getTiles()->get(2), new UploadedFile($this->tools->generateFakeImage(256, 256), '0.png', 'image/png', null, true));
        $this->tools->validateTile($frame->getTiles()->get(3), new UploadedFile($this->tools->generateFakeImage(256, 256), '0.png', 'image/png', null, true));

        $this->assertTrue($this->frameService->generateImage($frame));
        $this->assertFileExists($this->frameRepository->getStorageDirectory($frame).DIRECTORY_SEPARATOR.sprintf("%04d", $frame->getNumber()).'.'.$frame->getImageExtension());
    }

    public function testOnFinishTileForFrameLayer() {

        $blendId = $this->toolsService->addBlend("layer", 1, 10, 1920, 1080);
        $this->assertTrue(is_int($blendId));

        $this->blend = $this->blendRepository->find($blendId);
        $this->assertNotNull($this->blend);

        $frame = $this->blend->getFrames()->get(0);
        $this->tools->validateTile($frame->getTiles()->get(0), new UploadedFile($this->tools->generateFakeImage(256, 256), '0.png', 'image/png', null, true));
        $this->tools->validateTile($frame->getTiles()->get(1), new UploadedFile($this->tools->generateFakeImage(256, 256), '0.png', 'image/png', null, true));
        $this->tools->validateTile($frame->getTiles()->get(2), new UploadedFile($this->tools->generateFakeImage(256, 256), '0.png', 'image/png', null, true));
        $this->tools->validateTile($frame->getTiles()->get(3), new UploadedFile($this->tools->generateFakeImage(256, 256), '0.png', 'image/png', null, true));
        $this->tools->validateTile($frame->getTiles()->get(4), new UploadedFile($this->tools->generateFakeImage(256, 256), '0.png', 'image/png', null, true));
        $this->tools->validateTile($frame->getTiles()->get(5), new UploadedFile($this->tools->generateFakeImage(256, 256), '0.png', 'image/png', null, true));
        $this->tools->validateTile($frame->getTiles()->get(6), new UploadedFile($this->tools->generateFakeImage(256, 256), '0.png', 'image/png', null, true));
        $this->tools->validateTile($frame->getTiles()->get(7), new UploadedFile($this->tools->generateFakeImage(256, 256), '0.png', 'image/png', null, true));
        $this->tools->validateTile($frame->getTiles()->get(8), new UploadedFile($this->tools->generateFakeImage(256, 256), '0.png', 'image/png', null, true));
        $this->tools->validateTile($frame->getTiles()->get(9), new UploadedFile($this->tools->generateFakeImage(256, 256), '0.png', 'image/png', null, true));

        $this->assertTrue($this->frameService->generateImage($frame));
        $this->assertFileExists($this->frameRepository->getStorageDirectory($frame).DIRECTORY_SEPARATOR.sprintf("%04d", $frame->getNumber()).'.'.$frame->getImageExtension());
    }

    public function testResolutionPerTileForFrameFull10() {
        $blendId = $this->toolsService->addBlend("full", 10, 10, 1920, 1080);
        $this->assertTrue(is_int($blendId));

        $this->blend = $this->blendRepository->find($blendId);
        $this->assertNotNull($this->blend);

        $frame = $this->blend->getFrames()->get(0);

        $this->assertEquals(1920, $this->frameService->getWidthPerTile($frame));
        $this->assertEquals(1080, $this->frameService->getHeightPerTile($frame));

    }

    public function testResolutionPerTileForFrameFull20() {
        $blendId = $this->toolsService->addBlend("full", 10, 20, 1920, 1080);
        $this->assertTrue(is_int($blendId));

        $this->blend = $this->blendRepository->find($blendId);
        $this->assertNotNull($this->blend);

        $frame = $this->blend->getFrames()->get(0);

        $this->assertEquals(1920, $this->frameService->getWidthPerTile($frame));
        $this->assertEquals(1080, $this->frameService->getHeightPerTile($frame));

    }

    public function testResolutionPerTileForFrameRegion4() {
        $blendId = $this->toolsService->addBlend("region", 1, 4, 1920, 1080);
        $this->assertTrue(is_int($blendId));

        $this->blend = $this->blendRepository->find($blendId);
        $this->assertNotNull($this->blend);

        $frame = $this->blend->getFrames()->get(0);

        $this->assertEquals(1920 / 2, $this->frameService->getWidthPerTile($frame));
        $this->assertEquals(1080 / 2, $this->frameService->getHeightPerTile($frame));
    }

    public function testResolutionPerTileForFrameRegion16() {
        $blendId = $this->toolsService->addBlend("region", 1, 16, 1920, 1080);
        $this->assertTrue(is_int($blendId));

        $this->blend = $this->blendRepository->find($blendId);
        $this->assertNotNull($this->blend);

        $frame = $this->blend->getFrames()->get(0);

        $this->assertEquals(1920 / 4, $this->frameService->getWidthPerTile($frame));
        $this->assertEquals(1080 / 4, $this->frameService->getHeightPerTile($frame));
    }

    public function testResolutionPerTileForFrameLayer10() {
        $blendId = $this->toolsService->addBlend("layer", 1, 10, 1920, 1080);
        $this->assertTrue(is_int($blendId));

        $this->blend = $this->blendRepository->find($blendId);
        $this->assertNotNull($this->blend);

        $frame = $this->blend->getFrames()->get(0);

        $this->assertEquals(1920, $this->frameService->getWidthPerTile($frame));
        $this->assertEquals(1080, $this->frameService->getHeightPerTile($frame));
    }

    public function testResolutionPerTileForFrameLayer20() {
        $blendId = $this->toolsService->addBlend("layer", 1, 20, 1920, 1080);
        $this->assertTrue(is_int($blendId));

        $this->blend = $this->blendRepository->find($blendId);
        $this->assertNotNull($this->blend);

        $frame = $this->blend->getFrames()->get(0);

        $this->assertEquals(1920, $this->frameService->getWidthPerTile($frame));
        $this->assertEquals(1080, $this->frameService->getHeightPerTile($frame));
    }


}